import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { FavoritesPage } from '../favorites/favorites';
import { LibraryPage } from '../library/library';

/**
 * Generated class for the TabsPage tabs.
 *
 * See https://angular.io/docs/ts/latest/guide/dependency-injection.html for
 * more info on providers and Angular DI.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  libraryRoot = LibraryPage;
  favoritesRoot = FavoritesPage;


  constructor(public navCtrl: NavController) {}

}
